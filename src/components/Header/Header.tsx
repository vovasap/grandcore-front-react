import React, { FC } from 'react'
import { ButtonState, ButtonType } from 'types/Button'
import Logo from 'components/Logo/Logo'
import Navbar from 'components/Navbar/Navbar'
import Button from 'components/Button/Button'
import styles from 'components/Header/Header.module.scss'

interface IProps {}

const Header: FC<IProps> = () => {
  return <header className={styles.header}>
    <Logo />

    <Navbar />

    <Button type={ButtonType.PRIMARY} state={ButtonState.DEFAULT} text='Войти' />
  </header>
}

export default Header
