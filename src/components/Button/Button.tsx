import React, { FC } from 'react'
import { ButtonState, ButtonType } from 'types/Button'
import styles from 'components/Button/Button.module.scss'

interface IProps {
  type: ButtonType
  state: ButtonState
  text: string
  onClick?: () => any
}

const Button: FC<IProps> = (
  {
    type,
    state,
    onClick,
    text
  }
) => {
  const clickEvent = onClick ? () => onClick() : undefined,
        isDisabled = state === 'disabled'

  return <button className={ styles[type] } onClick={ clickEvent } disabled={ isDisabled } children={ text } />
}

export default Button
