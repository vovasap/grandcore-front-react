import React, { FC } from 'react'
import { IRouteLink } from 'types/Route'
import { Link } from 'react-router-dom'
import styles from 'components/Navbar/Navbar.module.scss'

interface IProps {
  route: IRouteLink
}

const NavbarLink: FC<IProps> = ({ route: { path, text } }) => {
  return <li className={styles.navbar__item}>
    <Link to={path} children={text} />
  </li>
}

export default NavbarLink
