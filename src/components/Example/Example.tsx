// This is example of Functional Component
import React, { FC } from 'react'
// styles with hashing
import styles from 'components/Example/Example.module.scss'

// Use interface to type Props or something you need with I-prefix
interface IProps {}

// Type component as FC and pass to them props types declared earlier
const Logo: FC<IProps> = () => {
  return <div className={styles.example} />
}

// Function Component file should have only one default export
export default Logo
