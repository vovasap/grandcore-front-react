import React, { FC } from 'react'
import MainLayout from './views/Main/Main'

interface IProps {}

const App: FC<IProps> = () => {
  return <MainLayout />
}

export default App
