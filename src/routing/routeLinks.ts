import { IRouteLink } from 'types/Route'

export const routesLinks: Array<IRouteLink> = [
  {
    path: '/auth',
    text: 'Auth Page'
  }
]
