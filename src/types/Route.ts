export interface IRoute {
    exact: boolean,
    path: string,
    component: any
}

export interface IRouteLink {
    path: string,
    text: string
}
